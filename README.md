# LMCMS
## Name

Leo's Minecraft Management System

## Description

A desktop Linux application developed using Gambas 3.18.0 to allow management of a locally installed Minecraft Forge client and remote Minecraft Forge server.  This application also facilitates the installation of both client and server mods sourced from 9Minecraft, synchronization and deletion of mods on both client and server.   

## Installation
To install clone project, open project in Gambas and compile.  

## Usage

## Support
Send all support request to bergamo.leo@gmail.com

## Authors and acknowledgment

Leo C. Bergamo

## License
GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007

## Project status
Project is currently in alpha stage.  It is not suitable for production use.
